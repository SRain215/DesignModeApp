package com.sc.design.presenter;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

/**
 * Created by SRain on 2016/9/10 0010.
 * 用途：
 */
public abstract class BasePresenter<T> {

    protected Reference<T> mViewRef;
//    protected Reference<IRadioManager> mServiceRef;

    public void attachView(T view) {
        mViewRef = new WeakReference<T>(view);
    }

    protected T getView() {
        return mViewRef.get();
    }

    public boolean isViewAttached() {
        return mViewRef != null && mViewRef.get() != null;
    }

    public void detachView() {
        if (mViewRef != null) {
            mViewRef.clear();
            mViewRef = null;
        }
    }

//    public void attachService(IRadioManager service) {
//        mServiceRef = new WeakReference<IRadioManager>(service);;
//    }
//    public boolean isServiceAttached() {
//        return mServiceRef != null && mServiceRef.get() != null;
//    }
//    public void detachService() {
//        if (mServiceRef != null) {
//            mServiceRef.clear();
//            mServiceRef = null;
//        }
//    }
}
