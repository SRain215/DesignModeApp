package com.sc.design.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by SRain on 2016/9/15 0015.
 * 用途：
 */
public class TestBean implements Parcelable {

    /**
     * title : srain
     * age : 28
     * sex : 1
     */

    private String title;
    private int age;
    private int sex;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeInt(this.age);
        dest.writeInt(this.sex);
    }

    public TestBean() {
    }

    protected TestBean(Parcel in) {
        this.title = in.readString();
        this.age = in.readInt();
        this.sex = in.readInt();
    }

    public static final Parcelable.Creator<TestBean> CREATOR = new Parcelable.Creator<TestBean>() {
        @Override
        public TestBean createFromParcel(Parcel source) {
            return new TestBean(source);
        }

        @Override
        public TestBean[] newArray(int size) {
            return new TestBean[size];
        }
    };
}
