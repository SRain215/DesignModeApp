package com.sc.design.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by SRain on 2016/9/7 0007.
 * 用途：数据类型，保存设计模式知识点信息
 */
public class KernelBean implements Parcelable {

    private int id; // 保存数据库中的id
    private String title; // 知识点(设计模式)名称
    private String content; // 简介（用途、适用场景）
    private String kernel; // 要点(包含注意事项、使用步骤)

    public KernelBean(int id, String title, String content, String kernel) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.kernel = kernel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getKernel() {
        return kernel;
    }

    public void setKernel(String kernel) {
        this.kernel = kernel;
    }

    protected KernelBean(Parcel parcel) {
        this.id = parcel.readInt();
        this.title = parcel.readString();
        this.content = parcel.readString();
        this.kernel = parcel.readString();
    }

    public static final Creator<KernelBean> CREATOR = new Creator<KernelBean>() {
        @Override
        public KernelBean createFromParcel(Parcel in) {
            return new KernelBean(in);
        }

        @Override
        public KernelBean[] newArray(int size) {
            return new KernelBean[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(content);
        dest.writeString(kernel);
    }
}
