package com.sc.design.view;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sc.design.R;
import com.sc.utils.lib.AppUtils;
import com.sc.utils.lib.DensityUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

//import butterknife.BindView;
//import butterknife.ButterKnife;

/**
 * Created by SRain on 2016/9/11 0011.
 * 用途：
 */
public class TestActivity extends Activity {

    private static final String TAG = TestActivity.class.getSimpleName();

    @BindView(R.id.text_app_name)
    TextView textAppName;
    @BindView(R.id.img_test)
    ImageView imgTest;
    @BindView(R.id.text_density)
    TextView textDensity;
    @BindView(R.id.text_file)
    TextView textFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void initView() {
        textAppName.setText("AppName == " + AppUtils.getAppName(this)
                + "\npageName == " + AppUtils.getPackageName(this)
                + "\ninfo == " + AppUtils.getPackageInfo(this)
                + "\nVersionName == " + AppUtils.getVersionName(this));

        Point point = DensityUtils.getScreenSize(this);
        float scale = DensityUtils.getDensity(this);
        DisplayMetrics metrics = DensityUtils.getMetrics(this);
        String strDensity = "point == " + point.toString() + "\nscale == " + scale + "\nmetrics == " + metrics.toString();
        textDensity.setText(strDensity);
    }

    @OnClick({R.id.text_app_name, R.id.img_test, R.id.text_density})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_app_name:
                break;
            case R.id.img_test:
                break;
            case R.id.text_density:
                break;
        }
    }
}
