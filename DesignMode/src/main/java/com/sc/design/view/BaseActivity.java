package com.sc.design.view;

import android.app.Activity;
import android.os.Bundle;

import com.sc.design.presenter.BasePresenter;

/**
 * Created by SRain on 2016/9/10 0010.
 * 用途：
 */
public abstract class BaseActivity<V, T extends BasePresenter<V>> extends Activity {

    protected T mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = createPresenter();
        mPresenter.attachView((V) this);
    }

    protected abstract T createPresenter();

    @Override
    protected void onDestroy() {
        mPresenter.detachView();
        super.onDestroy();
    }
}
